import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, View, Text, FlatList, Dimensions } from 'react-native'
import { AddTodo } from '../components/AddTodo'
import { TodoContext } from '../context/todo/todoContext'

export const MainScreen = () => {
  const { addTodo, todos } = useContext(TodoContext)
  const [deviceWidth, setDeviceWidth] = useState(
    Dimensions.get('window').width - 30 * 2
  )

  useEffect(() => {
    const update = () => {
      const width =
        Dimensions.get('window').width - 30 * 2
      setDeviceWidth(width)
    }

    Dimensions.addEventListener('change', update)

    return () => {
      //Dimensions.removeEventListener('change', update)
      Dimensions.dimensionsSubscription?.remove()
    }
  })

  let content = (
    <FlatList
        keyExtractor={item => item.id.toString()}
        data={todos}
        renderItem={({ item }) => (
          <Text style={styles.message}>{item.title}</Text>
        )}
    />
  )

  if (todos.length === 0) {
    content = (
      <View style={styles.empty}>
        <Text>No items.</Text>
      </View>
    )
  }

  return (
    <View>
      <AddTodo onSubmit={addTodo} />
      {content}
    </View>
  )
}

const styles = StyleSheet.create({
  empty: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  message: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#eee',
    borderRadius: 5,
    marginBottom: 5
  }
})
