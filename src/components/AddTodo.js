import io from 'socket.io-client'
import React, { useState, useEffect } from 'react'
import {
  View,
  Button,
  StyleSheet,
  TextInput,
  Alert,
  Keyboard
} from 'react-native'

var socket

export const AddTodo = ({ onSubmit }) => {
  const [value, setValue] = useState('')
  useEffect(() => {
    socket = io("http://192.168.1.65:3000") // check IP address
    // When you run on Android emulator see console output:
    // Opening exp://192.168.1.65:19000 on Pixel_2_API_30

    socket.on("message", msg => { 
      onSubmit(msg)   
    })
  }, [])


  const pressHandler = () => {
    if (value.trim()) {
      socket.emit( "message", value )
      //onSubmit(value)
      setValue('')
      Keyboard.dismiss()
    } else {
      Alert.alert(
        "Chat App",
        "Message cannot be empty.",
        [
          {
            text: "OK", onPress: () => console.log("")
          }
        ]
      )
    }
  }

  return (
    <View style={styles.block}>
      <TextInput
        style={styles.input}
        onChangeText={setValue}
        value={value}
        placeholder='Enter message...'
        autoCorrect={false}
        autoCapitalize='none'
      />
      <Button title='Send' onPress={pressHandler} />
    </View>
  )
}

const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15
  },
  input: {
    width: '60%',
    padding: 10,
    borderStyle: 'solid',
    borderBottomWidth: 2,
    borderBottomColor: "#3949ab"
  }
})
