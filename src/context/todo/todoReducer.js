import { ADD_TODO } from '../types'

const handlers = {
  [ADD_TODO]: (state, { title }) => ({
    ...state,
    todos: [
      {
        id: Date.now().toString(),
        title
      },
      ...state.todos
    ]
  }),
  DEFAULT: state => state
}

export const todoReducer = (state, action) => {
  const handler = handlers[action.type] || handlers.DEFAULT
  return handler(state, action)
}
