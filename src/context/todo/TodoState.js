import React, { useReducer, useContext } from 'react'
import { TodoContext } from './todoContext'
import { todoReducer } from './todoReducer'
import { ADD_TODO } from '../types'

export const TodoState = ({ children }) => { 
  const initialState = {
    todos: [] // [{ id: '1', title: 'Выучить React Native' }]
  }

  const [state, dispatch] = useReducer(todoReducer, initialState)

  const addTodo = title => dispatch({ type: ADD_TODO, title })

  
  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        addTodo
      }}
    >
      {children}
    </TodoContext.Provider>
  )
}
