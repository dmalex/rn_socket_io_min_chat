import React from 'react'
import { View, StyleSheet } from 'react-native'
import { MainScreen } from './src/screens/MainScreen'
import { TodoState } from './src/context/todo/TodoState'

export default function App() {
  return (
    <View style={styles.container}>
      <TodoState>
        <MainScreen />
      </TodoState>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 20
  }
})
